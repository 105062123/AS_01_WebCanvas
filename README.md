# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Basic components
- **Setting button**<br>
Click to unfold the setting sidebar<br>
<img src="https://i.imgur.com/0Fb05NU.png"></img><br>
<img src="https://i.imgur.com/Kk4wrQB.png" width=500></img>

- **Basic control tools**<br>
**\- Brush**<br>
<img src="https://i.imgur.com/7ZCcZE2.png"></img><br>
<img src="https://i.imgur.com/mWTSCwt.png" width=500></img><br>
**\- Eraser**<br>
<img src="https://i.imgur.com/bettxAF.png"></img><br>
<img src="https://i.imgur.com/hlxCIQh.png" width=500></img><br>
**\- Color selector**<br>
<img src="https://i.imgur.com/f2GYUa0.png"></img><br>
   ### [Source](https://github.com/DavidDurman/FlexiColorPicker)
   (I changed the appearance myself.)<br><br>
**\- Simple menu (brush size)**<br>
<img src="https://i.imgur.com/1QpZbWr.png"></img><br>
- **Text input**<br>
**\- User can type texts on canvas**<br>
<img src="https://i.imgur.com/TIIxZbR.png" width=500></img><br>
<img src="https://i.imgur.com/maFDHFr.png" width=500></img><br>
**\- Font menu (typeface and size)**<br>
<img src="https://i.imgur.com/7edGVux.png" width=500></img><br>
- **Cursor icon**<br>
**\- The image should change according to the currently used tool**<br>
Done. But the cursor doesn't show up in creenshot. So there is no demonstration.
- **Refresh button**<br>
**\- Reset canvas**<br>
<img src="https://i.imgur.com/Eyxmzrh.png" width=500></img><br>
## Advance tools

- **Different brush shapes**<br>
**\- Line, Circle(Ellipse), rectangle and triangle**<br>
<img src="https://i.imgur.com/SgVLsQt.png"></img><br>
<img src="https://i.imgur.com/UqQo7JB.png" width=500></img><br><br>
- **Un/Re-do button**<br>
<img src="https://i.imgur.com/a7zyTbP.png"></img><br><br>
- **Image tool**<br>
**\- User can upload image and paste it**<br>
<img src="https://i.imgur.com/ZqRMc8g.png"></img><br><br>
- **Download**<br>
**\- Download current canvas as an image file**<br>
<img src="https://i.imgur.com/Hz0GCyY.png"></img><br>

## Other useful widgets

- **Shortcut**<br>
**\-Un/Re-do  ->  Ctrl + Z / Ctrl + Shift + Z**
- **Regular shape**<br>
When using Ellipse/Rectangle tool, dragging mouse with SHIFT button down can make a Circle/Square.
- **Transparent background**<br>
Though the background of Canvas is transparent by default. It's not that easy to comfirm when it looks like white. So I make a button to change the background of the Canvas container into a gridded one.<br>
before<br>
<img src="https://i.imgur.com/wDZgC5J.png" width=500></img><br>
after<br>
<img src="https://i.imgur.com/l5OeJHS.png" width=500></img><br>
- **Eyedropper tool**<br>
Can get the color of a pixel clicked in Canvas.<br>
<img src="https://i.imgur.com/USlC5lE.png"></img><br>



