var isTransparent = false;
function transparency(){
    if(isTransparent){
        canvas.style.backgroundImage = "url()";
        isTransparent = false;
        canvas.style.border = 0;
    }else{
        canvas.style.backgroundImage = "url('https://i.imgur.com/tUK1OJj.png')";
        isTransparent = true;
        canvas.style.border = 0;
    }
}

window.addEventListener('load',function(){
    canvas.addEventListener('mousedown',mouseDown);
    canvas.addEventListener('mousemove',mouseMove);
    canvas.addEventListener('mousout',mouseUp);
    canvas.addEventListener('mouseup',mouseUp);
});


var tool = {
        type: 'pencil',
        x: 0,
        y: 0,
        color: '#000000',
        size: 10,
        down: false,
        shift_down: false,
    },
    strokes = [],
    currentStroke = null,
    strokes_idx = -1;

function redraw () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.lineCap = 'round';
    for (var i = 0; i <= strokes_idx; i++) {
        var s = strokes[i];
        console.log(i + ', ' + s.type);
        
        if(s.type == 'eraser'){
            ctx.globalCompositeOperation="destination-out";
        }else{
            ctx.globalCompositeOperation="source-over";
        }

        if(s.type == 'clear'){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            continue;
        }else if(s.type == 'text'){
            ctx.font = s.font;
            ctx.fillStyle = s.color;
            ctx.fillText(s.text, s.cPoints[0].x, s.cPoints[0].y);
            continue;
        }else if(s.type == 'upload'){
            var canvasPic = new Image();
            canvasPic.src = s.URL;
            ctx.drawImage(canvasPic, 0, 0);
            continue;
        }

        ctx.strokeStyle = s.color;
        ctx.lineWidth = s.size;

        

        ctx.beginPath();

        if(s.type == 'rectangle'){
            ctx.lineJoin = 'miter';
            var p = s.cPoints[s.cPoints.length - 1], r_x, r_y, r_width, r_height;
            var q = s.shift_down[s.shift_down.length - 1];
            if(p.x > s.cPoints[0].x){
                r_x = s.cPoints[0].x;
                r_width = p.x - s.cPoints[0].x;
            }else{
                r_x = p.x;
                r_width = s.cPoints[0].x - p.x;
            }

            if(p.y > s.cPoints[0].y){
                r_y = s.cPoints[0].y;
                r_height = p.y - s.cPoints[0].y;
            }else{
                r_y = p.y;
                r_height = s.cPoints[0].y - p.y;
            }
            if(q.s_down){
                var r_small = r_width < r_height ? r_width : r_height;
                if(p.x < s.cPoints[0].x)r_x = s.cPoints[0].x - r_small;
                if(p.y < s.cPoints[0].y)r_y = s.cPoints[0].y - r_small;
                ctx.rect(r_x, r_y, r_small, r_small);
            }else{
                ctx.rect(r_x, r_y, r_width, r_height);
            }
        }else if(s.type == 'triangle'){
            ctx.lineJoin = 'round';
            var p = s.cPoints[s.cPoints.length - 1];
            ctx.moveTo(s.cPoints[0].x, p.y);
            ctx.lineTo(p.x, p.y);
            ctx.lineTo((s.cPoints[0].x + p.x)/2, s.cPoints[0].y);
            ctx.closePath();
        }else if(s.type == 'circle'){
            ctx.lineJoin = 'round';
            ctx.save();
            var p = s.cPoints[s.cPoints.length - 1];
            var q = s.shift_down[s.shift_down.length - 1];
            var c_width = Math.abs(s.cPoints[0].x - p.x);
            var c_height = Math.abs(s.cPoints[0].y - p.y);
            var c_radius = c_width < c_height ? c_width/2 : c_height/2;

            if(q.s_down){
                var c_x, c_y;
                if(p.x > s.cPoints[0].x) c_x = s.cPoints[0].x + c_radius;
                else c_x = s.cPoints[0].x - c_radius;
                if(p.y > s.cPoints[0].y) c_y = s.cPoints[0].y + c_radius;
                else c_y = s.cPoints[0].y - c_radius;
            }else{
                var c_x = (s.cPoints[0].x + p.x)/2;
                var c_y = (s.cPoints[0].y + p.y)/2;

                ctx.scale(c_width/(c_radius*2), c_height/(c_radius*2));
                c_x = c_x * (c_radius*2) / c_width;
                c_y = c_y * (c_radius*2) / c_height;
            }
            ctx.arc(c_x, c_y, c_radius, 0, 2*Math.PI);
            ctx.restore();
        }else{
            ctx.lineJoin = 'round';
            ctx.moveTo(s.cPoints[0].x, s.cPoints[0].y);

            if(s.type == 'pencil' || s.type == 'eraser'){
                for (var j = 0; j < s.cPoints.length; j++) {
                    var p = s.cPoints[j];
                    ctx.lineTo(p.x, p.y);
                }
            }else if(s.type == 'line'){
                var p = s.cPoints[s.cPoints.length - 1];
                ctx.lineTo(p.x, p.y);
            }
        }
        ctx.stroke();
    }
}

var canvas = document.getElementById("myCanvas"),
    ctx = canvas.getContext('2d'),
    defaultWidth = window.innerWidth*0.93,
    defaultHeight = window.innerHeight*0.87;

canvas.width = defaultWidth;
canvas.height = defaultHeight;


function changeTool(t){
    tool.type = t;
    if(t == 'bucket' || t == 'dropper'){
        $('#myRange').css("visibility", "hidden");
    }else{
        $('#myRange').css("visibility", "visible");
    }
    if(t == 'text'){
        $('#font-select').css("visibility", "visible");
    }else{
        $('#font-select').css("visibility", "hidden");
    }
}

var cp = ColorPicker(document.getElementById('color-picker'),updateInputs);
var iHex = document.getElementById('hex');
var iColorBox = document.getElementById('color-box');

function updateInputs(hex) {
    iColorBox.style.backgroundColor = iHex.value = tool.color = hex;
}
function updateColorPickers(hex) {
    iColorBox.style.backgroundColor = tool.color = hex;
    cp.setHex(hex);
}

var initialHex = '#000000';
updateColorPickers(initialHex);

iHex.onchange = function() { updateColorPickers(iHex.value); };

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function mouseEvent (e) {
    var o=canvas;
    canvas.offsetX=canvas.offsetLeft;
    canvas.offsetY=canvas.offsetTop;


    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    
    if(tool.type == 'text')return;

    if(tool.type == 'eraser'){
        ctx.globalCompositeOperation="destination-out";
    }else{
        ctx.globalCompositeOperation="source-over";
    }

    while(o.offsetParent){
        o=o.offsetParent;
        canvas.offsetX+=o.offsetLeft;
        canvas.offsetY+=o.offsetTop;
    }
    tool.x = e.pageX-canvas.offsetX;
    tool.y = e.pageY-canvas.offsetY;

    if(tool.type != 'dropper'){
        currentStroke.cPoints.push({
            x: tool.x,
            y: tool.y,
        });

        currentStroke.shift_down.push({
            s_down: tool.shift_down,
        })

        redraw();
    }else{
        var pixel = ctx.getImageData(tool.x, tool.y, 1, 1);
        var pdata = pixel.data;
        var newHex = rgbToHex(pdata[0], pdata[1], pdata[2]);
        updateColorPickers(newHex);
        updateInputs(newHex);
    }
}

function mouseDown(e){
    tool.down = true;
    

    if(tool.type != 'dropper' && tool.type != 'text'){
        currentStroke = {
            type: tool.type,
            color: tool.color,
            size: tool.size,
            cPoints: [],
            shift_down: [],
        };
        ++strokes_idx;
        if(strokes_idx < strokes.length)strokes.length = strokes_idx;
        strokes.push(currentStroke);
    }

    mouseEvent(e);
}

function mouseUp(e){
    tool.down = false;

    mouseEvent(e);

    currentStroke = null;
}

function mouseMove(e){
    if (tool.down)
        mouseEvent(e);
}

$('#myRange').on('input', function () {
    tool.size = this.value;
});

$('#save-btn').click(function () {
    window.open(canvas.toDataURL());
});

function undo(){
    if(strokes_idx>=0){
        --strokes_idx;
        redraw();
    }
}

function redo(){
    if(strokes_idx < strokes.length - 1){
        ++strokes_idx;
        redraw();
    }
}

$('#clear_canvas').click(function () {
    canvas.width = defaultWidth;
    canvas.height = defaultHeight;
    strokes_idx = -1;
    strokes.length = 0;
    $("a#tool-button").removeClass('selected');
    $("#tool-button").addClass('selected');
    tool.size = 5;
    tool.type = "pencil";
    updateInputs("#000000");
    updateColorPickers("#000000");
    document.getElementById("myRange").value = 5;
});

$("#myCanvas").hover(function(){
    if(tool.type == "dropper"){
        $(this).css("cursor", "url(img/dropper.ico) 0 20, auto");
    }else if(tool.type == "pencil"){
        $(this).css("cursor", "url(img/pencil.ico) 1 19, auto");
    }else if(tool.type == "eraser"){
        $(this).css("cursor", "url(img/eraser.ico) 5 19, auto");
    }else if(tool.type == "text"){
        $(this).css("cursor", "url(img/text.ico), auto");
    }else if(tool.type == "line" || tool.type == "circle" || tool.type == "rectangle" || tool.type == "triangle"){
        $(this).css("cursor", "crosshair");
    }else{
        $(this).css("cursor", "default");
    }
})


var ctrl_down = false;


$(document).keydown(function(e){
    var key_code = e.keyCode;
    if(key_code == 16){
        tool.shift_down = true;
    }
    if(key_code == 17){
        ctrl_down = true;
    }
    if(key_code == 90){
        if(ctrl_down){
            if(tool.shift_down){
                redo();
            }else{
                undo();
            }
        }
    }
})

$(document).keyup(function(e){
    var key_code = e.keyCode;
    if(key_code == 16){
        tool.shift_down = false;
    }
    if(key_code == 17){
        ctrl_down = false;
    }
})



var font = '50px sans-serif',
    font_name = 'Arial',
    hasInput = false;

canvas.onclick = function(e) {
    if (hasInput) return;
    if(tool.type == 'text')
    addInput(e.clientX, e.clientY);
}

function addInput(x, y) {
    
    var input = document.createElement('input');

    font = tool.size + 'px ' + font_name;
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';
    input.style.height = tool.size + 'px';
    input.style.font = font;
    input.style.color = tool.color;

    input.onkeydown = handleEnter;
    
    document.body.appendChild(input);

    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) {
    if(txt == '')return;

    var o=canvas;
    canvas.offsetX=canvas.offsetLeft;
    canvas.offsetY=canvas.offsetTop;

    while(o.offsetParent){
        o=o.offsetParent;
        canvas.offsetX+=o.offsetLeft;
        canvas.offsetY+=o.offsetTop;
    }
    var text_x = x-canvas.offsetX;
    var text_y = y-canvas.offsetY;


    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font;
    ctx.fillStyle = tool.color;
    ctx.fillText(txt, text_x, text_y - 4);

    currentStroke = {
        type: tool.type,
        color: tool.color,
        size: tool.size,
        cPoints: [],
        text: txt,
        font: font,
    };
    currentStroke.cPoints.push({
        x: text_x,
        y: text_y - 4,
    });
    ++strokes_idx;
    if(strokes_idx < strokes.length)strokes.length = strokes_idx;
    strokes.push(currentStroke);
}

function renew_font(f){
    if(f == 0){
        font_name = 'Arial';
    }else if(f == 1){
        font_name = 'Verdana';
    }else if(f == 2){
        font_name = 'Courier New';
    }else if(f == 3){
        font_name = 'serif';
    }else{
        font_name = 'sans-serif';
    }
}

$("#inputGroupFile01").change(function(){
   readURL(this);
 });

function readURL(input){
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function (e) {
            var canvasPic = new Image();
            canvasPic.src = e.target.result;
            canvasPic.onload = function () { 
                if(canvasPic.width < canvas.width)
                canvas.width = canvasPic.width;
                if(canvasPic.height < canvas.height)
                canvas.height = canvasPic.height;
                ctx.drawImage(canvasPic, 0, 0); 
            }
            currentStroke = {
                type: 'upload',
                URL: e.target.result,
                size: tool.size,
                cPoints: [],
            };
            ++strokes_idx;
            if(strokes_idx < strokes.length)strokes.length = strokes_idx;
            strokes.push(currentStroke);

            redraw();
        }
        reader.readAsDataURL(input.files[0]);
    }
}
